from flask import Flask

import sqlite3
from sqlite3 import Error

import dataset

app = Flask(__name__)


@app.route("/")
def hello():
    return "Hello World!"

@app.route("/get-employes", methods=["POST"])
def get_user():
    data_employe = dataset.users

    for emp in data_employe:
        print(emp)
    