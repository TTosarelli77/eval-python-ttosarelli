from Models.Employe import Employe
from Models.Client import Client
from Models.Sejour import Sejour
from Models.Chambre import Chambre
from Models.TypeChambre import TypeChambre
from Models.Produit import Produit
from Models.Commande import Commande


users = [
  Employe(1,"Tosarelli", "prenom", "telephone","email", "adresse", "login", "password","service", 1),
  Employe(2,"Manuel ", "Micael ", "telephone 2 ","email 2 ", "adresse 2 ", "login 2 ", "password 2 ","service 2 ", 0)
]

clients = [
  Client(1,"client1", 'test',"0638509725", "9808765432134543", "Rue Pierre de Maupertuit")
]

categories = [
  TypeChambre(1,"suite-senior", 60),
  TypeChambre(2,"simple confort", 20),
  TypeChambre(3,"moyen confort", 40),
  TypeChambre(4,"suite junior", 50),
]

chambres = [
  Chambre(categories[0].serialize())
]

produits = [
  Produit(1,"coca", 1),
  Produit(2,"Sprite", 1),
  Produit(3,"pepsi", 1),
]

commandes  = [
  Commande(clients[0].serialize(), "14/05/2019", 300)
]

sejour = [
  Sejour(users[0].serialize(), "05/05/2019", "09/05/2019",clients[0].serialize(),chambres[0])
]