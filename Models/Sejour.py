from sqlalchemy import Column, String, Integer, ForeignKey, Numeric, Date
from sqlalchemy.orm import relationship
from Models.Model import Model

class Sejour(Model):
   __tablename__ = 'sejour'
   id = Column(Integer, primary_key=True, nullable=False,autoincrement=True)
   date_arrivees = Column(Date, nullable=False)
   date_depart = Column(Date, nullable=False)
   chambres = relationship("Chambre")
   clients = relationship("Client")
   employes = relationship("Employe")