from sqlalchemy import Column, String, Integer, ForeignKey, Numeric, Date
from sqlalchemy.orm import relationship
from Models.Model import Model

class Commande(Model):
   __tablename__ = 'commande'
   id = Column(Integer, primary_key=True, nullable=False,autoincrement=True)
   libelle = Column(String(200), nullable=False)
   prix_commande = Column(Integer, nullable=False)
   date = Column(Date, nullable=False)
   clients = relationship("Client")