from sqlalchemy import Column, String, Integer, ForeignKey, Numeric, Date
from sqlalchemy.orm import relationship
from Models.Model import Model

class TypeChambre(Model):
   __tablename__ = 'type_chambre'
   id = Column(Integer, primary_key=True, nullable=False,autoincrement=True)
   libelle = Column(String(200), nullable=False)
   prix = Column(Integer, nullable=False)