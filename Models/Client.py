from sqlalchemy import Column, String, Integer, ForeignKey, Numeric, Date
from sqlalchemy.orm import relationship
from Models.Model import Model

class Client(Model):
   __tablename__ = 'client'
   id = Column(Integer, primary_key=True, nullable=False,autoincrement=True)
   nom = Column(String(200), nullable=False)
   prenom = Column(String(200), nullable=False)
   numero_carte_telephone = Column(String(200), nullable=False)
   numero_carte_bancaire = Column(String(200), nullable=False)
   adresse = Column(String(200), nullable=False)
