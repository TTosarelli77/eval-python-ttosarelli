from sqlalchemy import Column, String, Integer, ForeignKey, Numeric, Date
from sqlalchemy.orm import relationship
from Models.Model import Model

class Produit(Model):
   __tablename__ = 'produit'
   id = Column(Integer, primary_key=True, nullable=False,autoincrement=True)
   nom = Column(String(200), nullable=False)
   prix = Column(Integer, nullable=False)
   commandes = relationship("Commande")