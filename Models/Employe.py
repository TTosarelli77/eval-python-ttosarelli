from sqlalchemy import Column, String, Integer, ForeignKey, Numeric, Date
from sqlalchemy.orm import relationship
from Models.Model import Model

class Employe(Model):
   __tablename__ = 'employes'
   id = Column(Integer, primary_key=True, nullable=False,autoincrement=True)
   nom = Column(String(200), nullable=False)
   prenom = Column(String(200), nullable=False)
   telephone = Column(String(200), nullable=False)
   email = Column(String(200), nullable=False)
   login = Column(String(200), nullable=False)
   mdp = Column(String(200), nullable=False)
   actif = Column(Integer, nullable=False)
