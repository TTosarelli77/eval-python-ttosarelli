from sqlalchemy import Column, String, Integer, ForeignKey, Numeric, Date
from sqlalchemy.orm import relationship
from Models.Model import Model

class Chambre(Model):
   __tablename__ = 'chambre'
   id = Column(Integer, primary_key=True, nullable=False,autoincrement=True)
   nom = Column(String(200), nullable=False)
   hotels = relationship("Hotel")
   types = relationship("TypeChambre")
