from sqlalchemy import Column, String, Integer, ForeignKey, Numeric, Date
from sqlalchemy.orm import relationship
from Models.Model import Model

class Hotel(Model):
   __tablename__ = 'hotel'
   id = Column(Integer, primary_key=True, nullable=False,autoincrement=True)
   nom = Column(String(200), nullable=False)